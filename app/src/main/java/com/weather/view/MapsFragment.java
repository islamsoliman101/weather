package com.weather.view;

import android.location.Address;
import android.location.Geocoder;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.TextView;

import com.google.android.gms.common.api.Status;
import com.google.android.gms.location.places.Place;
import com.google.android.gms.location.places.ui.PlaceSelectionListener;
import com.google.android.gms.location.places.ui.SupportPlaceAutocompleteFragment;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.weather.R;
import com.weather.viewmodel.MapsViewModel;

import java.io.IOException;
import java.util.List;
import java.util.Locale;

public class MapsFragment extends Fragment implements OnMapReadyCallback, GoogleMap.OnMapClickListener, GoogleMap.OnCameraMoveListener {

    private GoogleMap map;
    private Marker currentMarker;
    private TextView coordinates;
    private double lat;
    private double lng;
    private TextView longitude;
    private TextView latitude;
    private TextView addressTv;
    private FrameLayout locationInfoLayout;
    private MapsViewModel mapsViewModel;
    private FloatingActionButton saveLocationBtn;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.maps_fragment, container, false);
        mapsViewModel = new MapsViewModel();
        initUI(view);

        return view;
    }

    private void initUI(View view) {
        coordinates = view.findViewById(R.id.coordinates);
        longitude = view.findViewById(R.id.longitude);
        latitude = view.findViewById(R.id.latitude);
        locationInfoLayout = view.findViewById(R.id.location_info);
        addressTv = view.findViewById(R.id.address);
        saveLocationBtn = (FloatingActionButton) view.findViewById(R.id.btnSave);
        saveLocationBtnListiner();

        // Obtain the SupportMapFragment and get notified when the map is ready to be used.
        SupportMapFragment mapFragment = (SupportMapFragment) getChildFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);

        SupportPlaceAutocompleteFragment autocompleteFragment = (SupportPlaceAutocompleteFragment)
                getChildFragmentManager().findFragmentById(R.id.place_autocomplete_fragment);


        autocompleteFragment.setOnPlaceSelectedListener(new PlaceSelectionListener() {
            @Override
            public void onPlaceSelected(Place place) {
                map.clear();
                setNewMapMarker(place.getLatLng());
                map.moveCamera(CameraUpdateFactory.newLatLng(place.getLatLng()));
                map.animateCamera(CameraUpdateFactory.newLatLngZoom(place.getLatLng(), 12.0f));
                setCoordinatesInfo(place.getLatLng());

            }

            @Override
            public void onError(Status status) {

            }
        });
    }


    /**
     * Manipulates the map once available.
     * This callback is triggered when the map is ready to be used.
     * This is where we can add markers or lines, add listeners or move the camera. In this case,
     * we just add a marker near Sydney, Australia.
     * If Google Play services is not installed on the device, the user will be prompted to install
     * it inside the SupportMapFragment. This method will only be triggered once the user has
     * installed Google Play services and returned to the app.
     */
    @Override
    public void onMapReady(GoogleMap googleMap) {
        map = googleMap;
        LatLng cairo = new LatLng(30.037043448, 31.229631453);
        setNewMapMarker(cairo);
        map.moveCamera(CameraUpdateFactory.newLatLng(cairo));
        map.animateCamera(CameraUpdateFactory.newLatLngZoom(cairo, 9.0f));
        map.setOnMapClickListener(this);
        map.setOnCameraMoveListener(this);
    }

    @Override
    public void onCameraMove() {
     /*   Handler handler = new Handler();
        handler.postDelayed(new Runnable() {
            public void run() {
                setNewMapMarker(map.getCameraPosition().target);
                setCoordinatesInfo(map.getCameraPosition().target);
            }
        }, 2000);   //2 seconds
*/
    }

    @Override
    public void onMapClick(LatLng latLng) {
        setNewMapMarker(latLng);
        setCoordinatesInfo(latLng);

    }

    private void setNewMapMarker(LatLng latLng) {
        if (map != null) {
            if (currentMarker != null) {
                currentMarker.remove();
            }
            currentMarker = addMarker(latLng);

        }
    }

    private Marker addMarker(LatLng latLng) {
        return map.addMarker(new MarkerOptions().position(latLng).draggable(true));
    }

    private void showCoordinatesLayout() {
        longitude.setVisibility(View.VISIBLE);
        latitude.setVisibility(View.VISIBLE);
        coordinates.setVisibility(View.VISIBLE);
        changeLocationInfoLayoutVisibility(View.VISIBLE);
    }

    private void changeLocationInfoLayoutVisibility(int visibility) {
        locationInfoLayout.setVisibility(visibility);
    }

    private void setCoordinatesInfo(LatLng latLng) {
        this.latitude.setText(String.format("%s: %s", "Latitude", latLng.latitude));
        this.longitude.setText(String.format("%s: %s", "Longitude", latLng.longitude));
        this.lat = latLng.latitude;
        this.lng = latLng.longitude;
        Geocoder geocoder = new Geocoder(getActivity(), Locale.getDefault());
        List<Address> addresses = null;
        String address = "";
        try {
            addresses = geocoder.getFromLocation(latLng.latitude, latLng.longitude, 1);
            address = String.format("%s: %s", "Address", addresses.get(0).getAddressLine(0));
        } catch (IOException e) {
            e.printStackTrace();
        }
        this.addressTv.setText(address);
        showCoordinatesLayout();
    }

    private void saveLocationBtnListiner() {
        saveLocationBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                bookmarkLocation();
            }
        });
    }

    private void bookmarkLocation() {
        mapsViewModel.bookmarkLocation(this.addressTv.getText().toString(), this.lat, this.lng);
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mapsViewModel.closeDB();

    }
}
