package com.weather.view;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.widget.TextView;

import com.google.gson.Gson;
import com.weather.R;
import com.weather.model.entity.Weather;
import com.weather.viewmodel.CityViewModel;

/**
 * Created by Islam Soliman on 8/1/18.
 */

public class CityActivity extends Activity {
    String latitude;
    String longitude;
    public static final String LATITUDE = "latitude";
    public static final String LONGITUDE = "longitude";

    private TextView TVCityName;
    private TextView TVTemperature;
    private TextView TVHumidity;
    private TextView TVRain;
    private TextView TVWind;
    private CityViewModel cityViewModel;

    public static Intent newIntent(Context context) {
        return new Intent(context, CityActivity.class);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.city_activity);
        this.setFinishOnTouchOutside(false);
        initUI();
        cityViewModel = new CityViewModel();

        JSONWeatherTask task = new JSONWeatherTask();
        task.execute(new String[]{latitude, longitude});
    }

    private void initUI() {
        TVCityName = (TextView) findViewById(R.id.cityText);
        TVTemperature = (TextView) findViewById(R.id.temperature);
        TVHumidity = (TextView) findViewById(R.id.humidity);
        TVRain = (TextView) findViewById(R.id.rain);
        TVWind = (TextView) findViewById(R.id.wind);
        getdataIntent();
    }

    private void getdataIntent() {
        Bundle extras = getIntent().getExtras();
        if (extras != null) {
            latitude = extras.getString(LATITUDE);
            longitude = extras.getString(LONGITUDE);
        }
    }


    private class JSONWeatherTask extends AsyncTask<String, Void, Weather> {

        @Override
        protected Weather doInBackground(String... params) {
            String data = cityViewModel.getweatherData(params[0], params[1]);
            return new Gson().fromJson(data, Weather.class);
        }


        @Override
        protected void onPostExecute(Weather weather) {
            super.onPostExecute(weather);
            fillWeatherData(weather);

        }

    }

    private void fillWeatherData(Weather weather) {
        if (weather != null) {
            TVCityName.setText(weather.getName() + "," + weather.getSys().getCountry());
            TVTemperature.setText("" + Math.round((weather.getMain().getTemp() - 273.15)) + "C");
            TVHumidity.setText("" + weather.getMain().getHumidity() + "%");
            TVRain.setText("" + weather.getWeather().get(0).getDescription());
            TVWind.setText("" + weather.getWind().getSpeed() + " mps");

        }
    }
}
