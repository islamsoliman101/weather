package com.weather.view;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.weather.R;
import com.weather.adapter.LocationAdapter;
import com.weather.model.entity.Coordinate;
import com.weather.viewmodel.SavedLocationsViewModel;

import java.util.List;

/**
 * Created by Islam Soliman on 8/1/18.
 */

public class SavedLocationsFragment extends Fragment {

    private SavedLocationsViewModel savedLocationsViewModel;
    private RecyclerView locationListView;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.saved_locations_fragment, container, false);
        savedLocationsViewModel = new SavedLocationsViewModel();
        initUI(view);
        return view;
    }

    private void initUI(View view) {
        locationListView = (RecyclerView) view.findViewById(R.id.locationList);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getActivity());
        locationListView.setLayoutManager(linearLayoutManager);
        locationListView.setHasFixedSize(true);
    }

    @Override
    public void onResume() {
        super.onResume();
        setUpAdapter();
    }

    private void setUpAdapter() {
        List<Coordinate> allLocations = savedLocationsViewModel.getLocations();
        if (allLocations.size() > 0) {
            locationListView.setVisibility(View.VISIBLE);
            LocationAdapter mAdapter = new LocationAdapter(getActivity(), savedLocationsViewModel, allLocations);
            locationListView.setAdapter(mAdapter);

        } else {
            locationListView.setVisibility(View.GONE);
            Toast.makeText(getActivity(), "There is no Location in the database. Start adding now", Toast.LENGTH_LONG).show();
        }
    }


    @Override
    public void onDetach() {
        super.onDetach();
        savedLocationsViewModel.closeDB();
    }
}
