package com.weather;

import android.app.Application;
import android.content.Context;

/**
 * Created by Islam Soliman on 8/1/18.
 */

public class App extends Application {

    private static Context context;

    @Override
    public void onCreate() {
        super.onCreate();
        context = getApplicationContext();

    }


    public static Context getContext() {
        return context;
    }
}
