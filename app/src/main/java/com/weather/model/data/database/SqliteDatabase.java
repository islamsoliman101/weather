package com.weather.model.data.database;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import com.weather.model.entity.Coordinate;
import com.weather.view.Listener;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Islam Soliman on 8/1/18.
 */

public class SqliteDatabase extends SQLiteOpenHelper {

    private static final int DATABASE_VERSION = 1;
    private static final String DATABASE_NAME = "Locations";
    private static final String TABLE_Location = "Location";
    private static final String COLUMN_ID = "_id";
    private static final String COLUMN_LATLNG = "latlng";
    private static final String COLUMN_ADDRESS = "address";
    private static final String COLUMN_LATITUDE = "latitude";
    private static final String COLUMN_LONGITUDE = "longitude";

    public SqliteDatabase(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        String CREATE_LOCATION_TABLE = "CREATE	TABLE " + TABLE_Location + "(" + COLUMN_ID + " INTEGER PRIMARY KEY," + COLUMN_LATLNG + " DOUBLE," + COLUMN_ADDRESS + " TEXT," + COLUMN_LATITUDE + " DOUBLE," + COLUMN_LONGITUDE + " DOUBLE," + " UNIQUE(" + COLUMN_LATLNG + "))";
        db.execSQL(CREATE_LOCATION_TABLE);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_Location);
        onCreate(db);
    }

    public List<Coordinate> listLocations() {
        String sql = "select * from " + TABLE_Location;
        SQLiteDatabase db = this.getReadableDatabase();
        List<Coordinate> locations = new ArrayList<>();
        Cursor cursor = db.rawQuery(sql, null);
        if (cursor.moveToFirst()) {
            do {
                int id = Integer.parseInt(cursor.getString(0));
                double latlng = Double.parseDouble(cursor.getString(1));
                String address = cursor.getString(2);
                double latitude = Double.parseDouble(cursor.getString(3));
                double longitude = Double.parseDouble(cursor.getString(4));

                locations.add(new Coordinate(id, latlng, address, latitude, longitude));
            } while (cursor.moveToNext());
        }
        cursor.close();
        return locations;
    }

    public void addLocation(Coordinate location, Listener savesuccess) {
        ContentValues values = new ContentValues();
        values.put(COLUMN_LATLNG, location.getLatLng());
        values.put(COLUMN_ADDRESS, location.getAddress());
        values.put(COLUMN_LATITUDE, location.getLatitude());
        values.put(COLUMN_LONGITUDE, location.getLongitude());
        SQLiteDatabase db = this.getWritableDatabase();
        long result = db.insert(TABLE_Location, null, values);
        if (result == -1)
            savesuccess.addedBefore();
        else
            savesuccess.saveSuccessfuly();

    }

    public void deleteLocation(double id) {
        SQLiteDatabase db = this.getWritableDatabase();
        db.delete(TABLE_Location, COLUMN_ID + "	= ?", new String[]{String.valueOf(id)});
    }
}
