package com.weather.model.entity;


/**
 * Created by Islam Soliman on 8/1/18.
 */

public class Coordinate {
    private double latLng;
    private String address;
    private double latitude;
    private double longitude;
    private int id;


    public Coordinate(int id, double latLng, String address, double latitude, double longitude) {
        this.latLng = latLng;
        this.address = address;
        this.latitude = latitude;
        this.longitude = longitude;
        this.id = id;
    }

    public Coordinate(double latLng, String address, double latitude, double longitude) {
        this.latLng = latLng;
        this.address = address;
        this.latitude = latitude;
        this.longitude = longitude;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public double getLatLng() {
        return latLng;
    }

    public void setLatLng(int latLng) {
        this.latLng = latLng;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public double getLatitude() {
        return latitude;
    }

    public void setLatitude(double latitude) {
        this.latitude = latitude;
    }

    public double getLongitude() {
        return longitude;
    }

    public void setLongitude(double longitude) {
        this.longitude = longitude;
    }
}
