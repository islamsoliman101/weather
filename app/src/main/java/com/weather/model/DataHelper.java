package com.weather.model;

import com.weather.App;
import com.weather.model.data.database.SqliteDatabase;
import com.weather.model.data.network.WeatherHttpClient;
import com.weather.model.entity.Coordinate;
import com.weather.view.Listener;

import java.util.List;

/**
 * Created by Islam Soliman on 8/1/18.
 */

public class DataHelper {

    private SqliteDatabase mDatabase;
    private WeatherHttpClient weatherHttpClient;
    private static final DataHelper ourInstance = new DataHelper();

    public static DataHelper getInstance() {
        return ourInstance;
    }

    private DataHelper() {
        mDatabase = new SqliteDatabase(App.getContext());
        weatherHttpClient = new WeatherHttpClient();
    }

    public String getWeatherData(String latitude, String longitude) {
        return weatherHttpClient.getWeatherData(latitude, longitude);
    }

    public List<Coordinate> getLocations() {
        return mDatabase.listLocations();
    }

    public void removeLocation(double id) {
        mDatabase.deleteLocation(id);
    }

    public void addLocation(Coordinate newLocation, Listener savesuccess) {
        mDatabase.addLocation(newLocation, savesuccess);
    }

    public void closeDB() {
        if (mDatabase != null) {
            mDatabase.close();
        }
    }
}
