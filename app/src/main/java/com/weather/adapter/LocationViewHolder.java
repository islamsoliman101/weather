package com.weather.adapter;


import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.weather.R;

/**
 * Created by Islam Soliman on 8/1/18.
 */

public class LocationViewHolder extends RecyclerView.ViewHolder {

    public TextView address;
    public TextView latitude;
    public TextView longitude;
    public ImageView deleteLocation;
    public CardView view;
    public LocationViewHolder(View itemView) {
        super(itemView);
        address = (TextView) itemView.findViewById(R.id.locationAddress);
        latitude = (TextView) itemView.findViewById(R.id.locationLatitude);
        longitude = (TextView) itemView.findViewById(R.id.locationLongitude);
        deleteLocation = (ImageView) itemView.findViewById(R.id.deleteLocation);
        view = (CardView) itemView.findViewById(R.id.view);
    }
}
