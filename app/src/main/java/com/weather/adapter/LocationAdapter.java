package com.weather.adapter;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.weather.R;
import com.weather.model.entity.Coordinate;
import com.weather.view.CityActivity;
import com.weather.viewmodel.SavedLocationsViewModel;

import java.util.List;

/**
 * Created by Islam Soliman on 8/1/18.
 */

public class LocationAdapter extends RecyclerView.Adapter<LocationViewHolder> {

    private List<Coordinate> listLocations;
    private Context context;
    private SavedLocationsViewModel savedLocationsViewModel;

    public LocationAdapter(Context context, SavedLocationsViewModel savedLocationsViewModel, List<Coordinate> listLocations) {
        this.listLocations = listLocations;
        this.savedLocationsViewModel = savedLocationsViewModel;
        this.context = context;
    }

    @Override
    public LocationViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.location_item, parent, false);
        return new LocationViewHolder(view);
    }

    @Override
    public void onBindViewHolder(LocationViewHolder holder, final int position) {
        final Coordinate coordinate = listLocations.get(position);

        holder.address.setText(coordinate.getAddress());
        holder.latitude.setText(String.format("%s: %s", "Longitude", coordinate.getLatitude()));
        holder.longitude.setText(String.format("%s: %s", "Longitude", coordinate.getLongitude()));


        holder.deleteLocation.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                removeLocation(coordinate, position);
            }
        });

        holder.view.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startCityActivity(coordinate);
            }
        });


    }

    private void removeLocation(Coordinate coordinate, int position) {
        savedLocationsViewModel.removeLocation(coordinate.getId());
        listLocations.remove(position);
        notifyDataSetChanged();
    }

    private void startCityActivity(Coordinate coordinate) {
        Intent intent = CityActivity.newIntent(context);
        intent.putExtra(CityActivity.LATITUDE, String.valueOf(coordinate.getLatitude()));
        intent.putExtra(CityActivity.LONGITUDE, String.valueOf(coordinate.getLongitude()));
        context.startActivity(intent);
    }

    @Override
    public int getItemCount() {
        return listLocations.size();
    }

}
