package com.weather.viewmodel;

import com.weather.model.DataHelper;
import com.weather.model.entity.Coordinate;

import java.util.List;

/**
 * Created by Islam Soliman on 8/1/18.
 */

public class SavedLocationsViewModel {


    public SavedLocationsViewModel() {
    }

    public List<Coordinate> getLocations() {
        return DataHelper.getInstance().getLocations();
    }

    public void removeLocation(double id) {
        DataHelper.getInstance().removeLocation(id);
    }

    public void closeDB() {
        DataHelper.getInstance().closeDB();

    }
}
