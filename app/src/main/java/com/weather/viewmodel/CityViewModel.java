package com.weather.viewmodel;

import com.weather.model.DataHelper;

/**
 * Created by Islam Soliman on 8/2/18.
 */

public class CityViewModel {

    public CityViewModel() {

    }

    public String getweatherData(String latitude, String longitude) {
        return DataHelper.getInstance().getWeatherData(latitude, longitude);
    }

}
