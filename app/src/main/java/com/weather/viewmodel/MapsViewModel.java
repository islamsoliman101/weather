package com.weather.viewmodel;

import android.widget.Toast;

import com.weather.App;
import com.weather.model.DataHelper;
import com.weather.model.entity.Coordinate;
import com.weather.view.Listener;

/**
 * Created by Islam Soliman on 8/1/18.
 */

public class MapsViewModel implements Listener {
    private Listener savesuccess;

    public MapsViewModel() {
        savesuccess = this;
    }

    public void bookmarkLocation(String address, double latitude, double longitude) {
        Coordinate newLocation = new Coordinate(latitude + longitude, address, latitude, longitude);
        DataHelper.getInstance().addLocation(newLocation, savesuccess);
    }

    @Override
    public void saveSuccessfuly() {
        Toast.makeText(App.getContext(), "Save Location Successfully", Toast.LENGTH_SHORT).show();
    }

    @Override
    public void addedBefore() {
        Toast.makeText(App.getContext(), "Location Exist", Toast.LENGTH_SHORT).show();
    }


    public void closeDB() {
        DataHelper.getInstance().closeDB();

    }
}
